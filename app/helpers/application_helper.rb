module ApplicationHelper
 # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    main_title = "Resume: Tiffany A. Forsyth"
    if page_title.empty?
      main_title
    else
      page_title + " | " + main_title
    end
  end
end
