class DownloadsController < ApplicationController
  def download_pdf
		send_file(
    "#{Rails.root}/public/Resume.pdf",
    filename: "Resume.pdf",
    type: "application/pdf"
  )
  end
end
