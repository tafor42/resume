require 'rails_helper'
RSpec.feature "home page", type: :feature do
    describe "navigation" do
        it "displays navbar" do
            visit "/"
           
            expect(page.find('.nav')).to have_content("Skills")
        end
    end
    
		describe "buttons" do
			it "can download resume" do
				click("Download Resume")
				expect(page.find("")).to have_content("")
				
end
