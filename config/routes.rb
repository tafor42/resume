Rails.application.routes.draw do
  get 'downloads/download_pdf'

  root 'static_pages#home'
	get '/home', to: 'static_pages#home'
	get '/about', to: 'static_pages#about'
	get '/contact', to: 'static_pages#contact'
	get '/experience', to: 'static_pages#experience'
	get '/education', to: 'static_pages#education'
	get '/skills', to: 'static_pages#skills'
  get '/downloads', to: 'downloads#download_pdf'
end
