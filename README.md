# README

Ruby version: 2.3.1p112

To get started with the app, clone the repo and then install the needed gems:

$ bundle install --without production

Run the test suite to verify that everything is working correctly:

$ rails test

If the test suite passes, you'll be ready to run the app in a local server:

You may get a message in your console telling you to run rake db:migrate. This isn't necessary yet as this app currently isn't using a database. However, I've left the database configuration intact as I plan to add more functionality soon.