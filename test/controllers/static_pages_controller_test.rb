require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

	def setup
		@main_title = "Resume: Tiffany A. Forsyth"
	end

	test "should get home" do
    get root_path
    assert_response :success
		assert_select "title", "#{@main_title}"
  end

	test "should get about" do
		get about_path
		assert_response :success
		assert_select "title", "About | #{@main_title}"
	end

	test "should get contact" do
		get contact_path
		assert_response :success
		assert_select "title", "Contact | #{@main_title}"
	end

	test "should get experience" do
		get experience_path
		assert_response :success
		assert_select "title", "Experience | #{@main_title}"
	end

	test "should get education" do
		get education_path
		assert_response :success
		assert_select "title", "Education | #{@main_title}"
	end

	test "should get skills" do
		get skills_path
		assert_response :success
		assert_select "title", "Skills | #{@main_title}"
	end
end
